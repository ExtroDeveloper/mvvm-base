package com.extro.mvvm.activity

import android.app.Activity
import android.support.v4.app.FragmentActivity
import com.extro.mvvm.utils.BaseCommandRunner

/**
 * Исполнитель [com.extro.mvvm.utils.BaseCommand], использующий [Activity]
 */
open class CommandRunner : BaseCommandRunner<FragmentActivity>()