package com.extro.mvvm.activity

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.extro.mvvm.BaseViewModel
import com.extro.mvvm.Router
import com.extro.mvvm.fragment.BackPressListener
import com.extro.mvvm.utils.LifeCycleCallback
import com.extro.mvvm.utils.ViewModelAndRouter

/**
 * Базовый класс MVVM [Activity]
 *
 * @param <ROUTER> тип роутера
 * @param <VIEW_MODEL> тип вьюмодели
 * @param <BINDING> тип [ViewDataBinding], соответствующий layout-ресурсу [View] активности
</BINDING></VIEW_MODEL></ROUTER> */
abstract class VMActivity<ROUTER, VIEW_MODEL : BaseViewModel<in ROUTER>, BINDING : ViewDataBinding> :
    AppCompatActivity() where ROUTER : LifeCycleCallback<FragmentActivity>, ROUTER : Router {

    private var viewModelAndRouter: ViewModelAndRouter<ROUTER, VIEW_MODEL, FragmentActivity>? = null
    protected var binding: BINDING? = null

    @get:LayoutRes
    protected abstract val layoutId: Int

    protected val viewModel: VIEW_MODEL
        get() = viewModelAndRouter!!.viewModel

    //region Activity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)

        @Suppress("UNCHECKED_CAST")
        if (lastCustomNonConfigurationInstance is ViewModelAndRouter<*, *, *>) {
            viewModelAndRouter = lastCustomNonConfigurationInstance as ViewModelAndRouter<ROUTER, VIEW_MODEL, FragmentActivity>
        } else {
            viewModelAndRouter = ViewModelAndRouter(createViewModel(), createRouter())
            extractIntentArguments(intent)
            viewModelAndRouter!!.viewModel.initialize()
        }
        setBindingVariable(binding!!)
        viewModelAndRouter!!.attach(this)
    }

    abstract fun setBindingVariable(binding: BINDING)

    override fun onDestroy() {
        super.onDestroy()
        viewModelAndRouter!!.detach(this.lifecycle)
        viewModelAndRouter = null
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        extractIntentArguments(intent)
    }

    @CallSuper
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModelAndRouter!!.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Вызывает обработчик события нажатия кнопки "Назад" вьюмодели и выполняет обработку самостоятельно,
     * если оно не было обработано вьюмоделью
     */
    override fun onBackPressed() {
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is BackPressListener && fragment.isVisible) {
                if ((fragment as BackPressListener).onBackPressed()) {
                    return
                }
            }
        }
        if (!viewModelAndRouter!!.onBackPressed()) {
            super.onBackPressed()
        }
    }

    @CallSuper
    override fun onRetainCustomNonConfigurationInstance(): ViewModelAndRouter<ROUTER, VIEW_MODEL, FragmentActivity>? {
        return viewModelAndRouter
    }

    @CallSuper
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModelAndRouter!!.processRequestPermissionsResult(requestCode, permissions, grantResults)
    }
    //endregion

    protected open fun extractIntentArguments(intent: Intent?) {

    }

    protected abstract fun createViewModel(): VIEW_MODEL

    protected abstract fun createRouter(): ROUTER
}