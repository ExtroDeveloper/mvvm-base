package com.extro.mvvm.utils

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.content.Intent
import com.extro.mvvm.BaseViewModel
import com.extro.mvvm.Router

/**
 * Класс, содержащий вьюмодель и роутер определённых типов
 *
 * @param <ROUTER> тип роутера
 * @param <VIEW_MODEL> тип вьюмодели
</VIEW_MODEL></ROUTER> */
class ViewModelAndRouter<ROUTER, VIEW_MODEL : BaseViewModel<in ROUTER>, ACTIVITY_OR_FRAGMENT : LifecycleOwner>(
    val viewModel: VIEW_MODEL,
    val router: ROUTER
) where ROUTER : LifeCycleCallback<ACTIVITY_OR_FRAGMENT>, ROUTER : Router {

    init {
        viewModel.router = router
    }

    fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        router.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

    fun onBackPressed(): Boolean {
        return viewModel.onBackPressed()
    }

    fun processRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        router.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    fun attach(activityOrFragment: ACTIVITY_OR_FRAGMENT) {
        router.attach(activityOrFragment)
        activityOrFragment.lifecycle.addObserver(router)
    }

    fun detach(lifecycle: Lifecycle) {
        lifecycle.removeObserver(router)
    }
}