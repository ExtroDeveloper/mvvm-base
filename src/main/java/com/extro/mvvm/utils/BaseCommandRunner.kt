package com.extro.mvvm.utils

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Выполняет команды использующие Activity, Fragment или FragmentManager. Комманты будкт выполнены только во врмя того как Activity или Fragment будут в активном состоянии
 * Есть два варианты выполнения:
 * 1) выполнить сразу, если есть возможность
 * 2) выполнить срузу либо при первой возможности - коммнда быдет выполнена когда Activity или Fragment будут в активном состоянии
 */
open class BaseCommandRunner<ACTIVITY_OR_FRAGMENT : LifecycleOwner> : LifeCycleCallback<ACTIVITY_OR_FRAGMENT>() {

    private val postponedCommands = ConcurrentLinkedQueue<BaseCommand<ACTIVITY_OR_FRAGMENT>>()
    private var started = false

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        started = true
        while (!postponedCommands.isEmpty()) {
            postponedCommands.poll().run {
                activityOrFragment = this@BaseCommandRunner.activityOrFragment
                run()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun pause() {
        started = true
    }

    /**
     * Попытается выполнить команду сразу, если есть доступ к контексту,
     * иначе выполнит команду при получении доступа к контексту,
     * то есть когда Fragment или Activity появятся на экране
     * @param action код использующий контекст и/или FragmentManager
     */
    fun runCommandSticky(action: (ACTIVITY_OR_FRAGMENT) -> Unit) {
        val command = BaseCommand(action)
        if (started) {
            command.activityOrFragment = activityOrFragment
            command.run()
        } else {
            postponedCommands.add(command)
        }
    }

    /**
     * Попытается выполнить команду сразу если есть доступ к контексту
     * @param action код использующий контекст и FragmentManager
     */
    fun runCommand(action: (ACTIVITY_OR_FRAGMENT) -> Unit) {
        if (!started) {
            return
        }
        BaseCommand(action).run {
            activityOrFragment = this@BaseCommandRunner.activityOrFragment
            run()
        }
    }
}