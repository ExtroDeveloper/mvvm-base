package com.extro.mvvm.utils

import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Intent
import android.content.pm.PackageManager

/**
 * Обработчик callback'ов жизненного цикла
 *
 * @param <ACTIVITY_OR_FRAGMENT> тип обладателя жизненного цикла
</ACTIVITY_OR_FRAGMENT> */
abstract class LifeCycleCallback<ACTIVITY_OR_FRAGMENT : LifecycleOwner> : LifecycleObserver {

    protected var activityOrFragment: ACTIVITY_OR_FRAGMENT? = null
    /**
     * Callback уничтожения обладателя жизненного цикла
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        activityOrFragment = null
    }

    /**
     * Привязывает [activityOrFragment]
     *
     * @param activityOrFragment поставщик [android.content.Context], чей жизненный цикл привязывается
     */
    fun attach(
        activityOrFragment: ACTIVITY_OR_FRAGMENT
    ) {
        this.activityOrFragment = activityOrFragment
    }

    /**
     * Callback, соответствующий вызову [Activity.onActivityResult]
     *
     * @param requestCode код запроса
     * @param resultCode  код результата
     * @param data        данные результата
     */
    fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
    }

    /**
     * Callback, вызываемый при получении результата запроса разрешений
     *
     * @param requestCode  код запроса
     * @param permissions  запрошенные разрешения
     * @param grantResults результаты запроса соответствующих разрешений
     * ([PackageManager.PERMISSION_GRANTED], либо [PackageManager.PERMISSION_DENIED])
     */
    open fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
    }
}