package com.extro.mvvm.utils

/**
 * Команда для выполнения кода с заданными [CONTEXT_PROVIDER] и
 * [FragmentManager]
 *
 * @param <CONTEXT_PROVIDER> класс, предоставляющий [android.content.Context]
</CONTEXT_PROVIDER> */
/**
 * Создаёт команду
 *
 * @param action выполняемый код
 */
internal class BaseCommand<ACTIVITY_OR_FRAGMENT>(private val action: (ACTIVITY_OR_FRAGMENT) -> Unit) {

    var activityOrFragment: ACTIVITY_OR_FRAGMENT? = null

    /**
     * Выполняет [action], передавая в качестве аргументов [.activityOrFragment] и
     * [.fragmentManager]
     */
    fun run() {
        action.invoke(activityOrFragment!!)
    }
}