package com.extro.mvvm.fragment

import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.extro.mvvm.BaseViewModel
import com.extro.mvvm.Router
import com.extro.mvvm.utils.LifeCycleCallback
import com.extro.mvvm.utils.ViewModelAndRouter

/**
 * Базовый класс MVVM [Fragment]
 *
 * @param <ROUTER> тип роутера
 * @param <VIEW_MODEL> тип вьюмодели
 * @param <BINDING> тип [ViewDataBinding], соответствующий layout-ресурсу [View] фрагмента
</BINDING></VIEW_MODEL></ROUTER> */
abstract class VMFragment<ROUTER, VIEW_MODEL : BaseViewModel<in ROUTER>, BINDING : ViewDataBinding> : Fragment(),
    BackPressListener where ROUTER : LifeCycleCallback<Fragment>, ROUTER : Router {

    private var viewModelAndRouter: ViewModelAndRouter<ROUTER, VIEW_MODEL, Fragment>? = null
    protected var binding: BINDING? = null

    @get:LayoutRes
    protected abstract val layoutId: Int

    protected val viewModel: VIEW_MODEL
        get() = viewModelAndRouter!!.viewModel

    //region Fragment
    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        viewModelAndRouter = ViewModelAndRouter(createViewModel(), createRouter())
            .also {
                extractInitialArguments(arguments)
                viewModel.initialize()
                it.attach(this)
            }
    }

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), layoutId, container, false)
        setBindingVariable(binding!!)
        return binding!!.root
    }

    abstract fun setBindingVariable(binding: BINDING)

    @CallSuper
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        viewModelAndRouter!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        viewModelAndRouter!!.processRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModelAndRouter!!.detach(this.lifecycle)
        viewModelAndRouter = null
    }

    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @CallSuper
    override fun onDetach() {
        super.onDetach()
        fixIssueWithRepeatingTransitionAnimationAfterRotation()
    }
    //endregion
    /**
     * Делегирует обработку события нажатия кнопки "Назад" вьюмодели, либо дочерним фрагментам, реализующим
     * [BackPressListener]. Если никто из них его не обработал, то обрабатывает самостоятельно
     *
     * @return было ли обработано нажатие кнопки "Назад"
     */
    override fun onBackPressed(): Boolean {
        for (fragment in childFragmentManager.fragments) {
            if (fragment is BackPressListener && fragment.isVisible) {
                if ((fragment as BackPressListener).onBackPressed()) {
                    return true
                }
            }
        }
        if (!viewModelAndRouter!!.onBackPressed()) {
            val manager = childFragmentManager
            if (!manager.isStateSaved && manager.backStackEntryCount > 1) {
                manager.popBackStack()
                return true
            }
        }
        return false
    }

    protected abstract fun createViewModel(): VIEW_MODEL

    protected abstract fun createRouter(): ROUTER

    protected open fun extractInitialArguments(arguments: Bundle?) {

    }

    /**
     * Исправление бага, связанного с setRetainInstance, когда анимация показа фрагмента воспроизводиться повторно, после поворота экрана
     */
    private fun fixIssueWithRepeatingTransitionAnimationAfterRotation() {
        try {
            val field = Fragment::class.java.getDeclaredField("mAnimationInfo")
            field.isAccessible = true
            field.set(this, null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}