package com.extro.mvvm.fragment

/**
 * Callback нажатия кнопки "Назад"
 */
interface BackPressListener {
    fun onBackPressed(): Boolean
}