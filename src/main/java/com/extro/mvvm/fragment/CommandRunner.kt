package com.extro.mvvm.fragment

import android.support.v4.app.Fragment

import com.extro.mvvm.utils.BaseCommandRunner

/**
 * Исполнитель [com.extro.mvvm.utils.BaseCommand], использующий [Fragment]
 */
open class CommandRunner : BaseCommandRunner<Fragment>()