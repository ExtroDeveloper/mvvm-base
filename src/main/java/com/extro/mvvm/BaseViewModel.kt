package com.extro.mvvm

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Базовый класс вьюмодели экрана
 *
 * @param <ROUTER> тип используемого роутера
</ROUTER> */
open class BaseViewModel<ROUTER : Router> : ViewModel() {

    lateinit var router: ROUTER
    private val disposables = CompositeDisposable()

    /**
     * Вызывается сразу после конструктора, весь код начала работы с репозиториям,
     * интеракторам или роутерам нужно размещать здесь
     */
    open fun initialize() {}

    /**
     * Обрабатывает нажатие кнопки "Назад"
     *
     * @return true, если нажатие кнопки "Назад" было обработано
     */
    fun onBackPressed(): Boolean {
        return false
    }

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun destroy() {
        disposables.dispose()
    }
}